from django.urls import path

from . import views

app_name = 'story5_app'

urlpatterns = [
    path('', views.index, name='index'),
    path('tambah_matkul/', views.tambah_matkul, name='tambah_matkul'),
    path('matkul/<int:id>', views.detail_matkul, name='detail_matkul'),
    path('delete_matkul/<int:id>', views.hapus_matkul, name='hapus_matkul')
]
