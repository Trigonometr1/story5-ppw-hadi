from django.forms import ModelForm
from .models import MataKuliah

class MataKuliahForm(ModelForm):
    class Meta:
        model = MataKuliah
        fields = "__all__"