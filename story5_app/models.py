from django.db import models
from django.shortcuts import reverse

# Create your models here.
pilihan_semester = [
    ("Gasal 2019/2020", "Gasal 2019/2020"),
    ("Genap 2019/2020", "Genap 2019/2020"),
    ("Gasal 2020/2021", "Gasal 2020/2021"),
    ("Genap 2020/2021", "Genap 2020/2021"),
    ("Gasal 2021/2022", "Gasal 2021/2022"),
    ("Genap 2021/2022", "Genap 2021/2022"),
    ("Gasal 2022/2023", "Gasal 2022/2023"),
    ("Genap 2022/2023", "Genap 2022/2023")
]
class MataKuliah(models.Model):
    nama = models.TextField(max_length = 40)
    dosen = models.TextField(max_length = 40)
    jumlah_sks = models.IntegerField()
    deskripsi = models.TextField(max_length = 400)
    semester = models.TextField(choices = pilihan_semester)
    ruangan = models.TextField(max_length = 40)

    def __str__(self):
        return "{} ({})".format(self.nama, self.dosen)

    def get_absolute_url(self):
        return reverse("story5_app:detail_matkul", args=[self.id])