from django.shortcuts import render, redirect
from .models import MataKuliah
from .forms import MataKuliahForm
from django.contrib import messages

def index(request):
    context = {
        'mata_kuliah_list' : MataKuliah.objects.all().order_by("id"),
        'page' : 'index'
    }
    return render(request, 'story5_app/index.html', context)

def tambah_matkul(request):
    if request.method == 'POST':
        matkul_form = MataKuliahForm(request.POST)
        if matkul_form.is_valid():
            matkul_form.save()
            messages.add_message(request, messages.SUCCESS, 'Mata Kuliah {} berhasil ditambahkan'.format(request.POST['nama']))
        return redirect('story5_app:index')
    context = {
        'page' : 'tambah_matkul'
    }
    return render(request, 'story5_app/tambah_matkul.html', context)

def detail_matkul(request, id):
    context = {
        'mata_kuliah' : MataKuliah.objects.get(id=id)
    }
    return render(request, 'story5_app/detail_matkul.html', context)

def hapus_matkul(request, id):
    if request.method == 'POST':
        mata_kuliah = MataKuliah.objects.get(id=id)
        mata_kuliah.delete()
        messages.add_message(request, messages.SUCCESS, 'Mata Kuliah {} berhasil dihilangkan'.format(mata_kuliah.nama))
    return redirect('story5_app:index')
