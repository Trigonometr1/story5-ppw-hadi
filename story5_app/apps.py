from django.apps import AppConfig


class MainConfig(AppConfig):
    name = 'story5_app'
