from django import forms
from .models import Kegiatan, Peserta

class PesertaForm(forms.ModelForm):
    nama = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "class" : "form-control",
                "placeholder" : "Nama Peserta"
                }),
        label="")
    class Meta:
        model = Peserta
        fields = "__all__"

class KegiatanForm(forms.ModelForm):
    nama = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "class" : "form-control w-50 text-center",
                "placeholder" : "Nama Kegiatan"
            }),
        label=""
    )
    class Meta:
        model = Kegiatan
        # Di form cuma ingin input nama kegiatan saja
        # add peserta bisa memakai PesertaForm
        exclude = ["peserta"]