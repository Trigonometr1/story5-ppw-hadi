from django.test import TestCase, Client
from .models import Peserta, Kegiatan
from django.urls import reverse
from .views import index, tambah_peserta, tambah_kegiatan, hapus_peserta, detail_peserta, detail_kegiatan

# Create your tests here.
class TestModels(TestCase):
    def setUp(self):
        self.peserta_test = Peserta.objects.create(
            nama = "peserta test"
        )
        self.kegiatan_test = Kegiatan.objects.create(
            nama = "kegiatan test"
        )
        self.kegiatan_test.peserta.add(self.peserta_test)
    
    def test_nama_peserta(self):
        self.assertEquals(str(self.peserta_test), "peserta test")
    
    def test_nama_kegiatan(self):
        self.assertEquals(str(self.kegiatan_test), "kegiatan test")

    def test_absolute_url_peserta(self):
        self.assertEquals(self.peserta_test.get_absolute_url(), "/story6_app/detail_peserta/peserta-test")

    def test_absolute_url_kegiatan(self):
        self.assertEquals(self.kegiatan_test.get_absolute_url(), "/story6_app/detail_kegiatan/kegiatan-test")

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.index_url = reverse("story6_app:index")
        self.detail_kegiatan_url = reverse("story6_app:detail_kegiatan", args=["kegiatan-satu-test"])
        self.kegiatan = Kegiatan.objects.create(nama = "kegiatan satu test")
        self.peserta_tetap = Peserta.objects.create(nama = "peserta tetap")
        self.tambah_peserta_url = reverse("story6_app:tambah_peserta", args=[self.kegiatan.id])
        self.detail_peserta_url = reverse("story6_app:detail_peserta", args=["peserta tetap"])
        self.tambah_kegiatan_url = reverse("story6_app:tambah_kegiatan")
        self.peserta_akan_dihapus = Peserta.objects.create(nama="hapus saya")
        self.hapus_peserta_url = reverse("story6_app:hapus_peserta")

    def test_index_GET(self):
        response = self.client.get(self.index_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "story6_app/index.html")
        self.assertContains(response, "Pengaturan Kegiatan")
    
    def test_detail_kegiatan_GET(self):
        response = self.client.get(self.detail_kegiatan_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "story6_app/detail_kegiatan.html")
    
    def test_detail_peserta_GET(self):
        response = self.client.get(self.detail_peserta_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "story6_app/detail_peserta.html")
    
    def test_tambah_peserta_POST_adds_new_name(self):
        response = self.client.post(self.tambah_peserta_url, {"nama" : "test_nama"})
        self.assertEquals(response.status_code,302)
        self.assertEquals(self.kegiatan.peserta.all()[0].nama,"test_nama")
    
    def test_tambah_peserta_POST_if_name_on_database(self):
        response = self.client.post(self.tambah_peserta_url, {"nama" : "peserta tetap"})
        self.assertEquals(response.status_code, 302)
        self.assertEquals(self.kegiatan.peserta.all()[0].nama, "peserta tetap")
    
    def test_tambah_peserta_POST_if_same_name(self):
        self.client.post(self.tambah_peserta_url, {"nama" : "test_nama_1"})
        response = self.client.post(self.tambah_peserta_url, {"nama" : "test_nama_1"})
        self.assertEquals(response.status_code, 302)
        self.assertEquals(len(self.kegiatan.peserta.all()), 1)

    def test_tambah_kegiatan_GET(self):
        response = self.client.get(self.tambah_kegiatan_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "story6_app/tambah_kegiatan.html")

    def test_tambah_kegiatan_POST_redirect(self):
        response = self.client.post(self.tambah_kegiatan_url, {"nama" : "test kegiatan"})
        self.assertEquals(response.status_code, 302)

    def test_hapus_peserta_POST(self):
        # tambahkan peserta
        self.kegiatan.peserta.add(self.peserta_akan_dihapus)
        old_length = len(self.kegiatan.peserta.all())

        # proses menghapus
        response = self.client.post(self.hapus_peserta_url, {
            "nama" : "hapus saya",
            "kegiatan_id" : self.kegiatan.id
        })
        self.assertEquals(len(self.kegiatan.peserta.all()), old_length-1)
        self.assertEquals(response.status_code, 302)
    
    def test_hapus_peserta_POST_peserta_not_found(self):
        response = self.client.post(self.hapus_peserta_url, {
            "nama" : "nama tidak ada",
            "kegiatan_id" : self.kegiatan.id
        }, follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, "tidak terdaftar di kegiatan")

