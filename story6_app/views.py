from django.shortcuts import render, redirect, reverse, get_object_or_404
from .models import Peserta, Kegiatan
from .forms import PesertaForm, KegiatanForm
from django.contrib import messages
# Create your views here.

def index(request):
    context = {
        "kegiatan_items" : Kegiatan.objects.all(),
        "peserta_form" : PesertaForm,
        "is_index" : True, # Untuk memberi CSS style
        "is_story6_app" : True
    }
    return render(request, "story6_app/index.html", context)

def tambah_peserta(request, id):
    if request.method == "POST":
        kegiatan = Kegiatan.objects.get(id=id)
        nama_peserta_baru = request.POST["nama"]
        # Supaya bisa memilih penambahan peserta yang sesuai kriteria
        # peserta_baru tipenya Peserta, status_baru tipenya boolean
        peserta_baru, status_baru = Peserta.objects.get_or_create(nama=nama_peserta_baru)
        if status_baru:
            # Tambah peserta baru yang belum ada sama sekali
            peserta_baru.save()
            # Masukkan peserta ke kegiatan yang sesuai
            kegiatan.peserta.add(peserta_baru)
            messages.add_message(request, messages.SUCCESS, "Peserta {} ditambahkan ke {}".format(nama_peserta_baru, kegiatan.nama))
        else: # Ada 2 kemungkinan, sudah ada di kegiatan itu
        #atau belum ada tapi sudah ada di database.
            try:
                # Coba cari adakah peserta di kegiatan ini
                kegiatan.peserta.get(nama=nama_peserta_baru) # Line ini akan error jika tidak ada pesertanya
                # jika tidak error, tandanya sudah ada peserta dengan nama nama_peserta_baru, jangan tambahkan.
                messages.add_message(request, messages.WARNING, "Peserta {} sudah ada di kegiatan {}".format(nama_peserta_baru, kegiatan.nama))
            except:
                # Kalau tadi error, maka nama sudah ada di kegiatan lain, tapi disini belum
                # Boleh ditambahkan
                kegiatan.peserta.add(peserta_baru)
                messages.add_message(request, messages.SUCCESS, "Peserta {} ditambahkan ke {}".format(nama_peserta_baru, kegiatan.nama))
    # redirect("/detail_kegiatan/nama-kegiatan")
    return redirect(reverse("story6_app:detail_kegiatan", args=[kegiatan.nama.replace(" ", "-")]))

def tambah_kegiatan(request):
    if request.method == "POST":
        kegiatan_form = KegiatanForm(request.POST)
        if kegiatan_form.is_valid():
            # kegiatan_object cuma buat ambil nama kegiatan
            kegiatan_object = kegiatan_form.save(commit=False)
            # Dibetulin format namanya
            nama_kegiatan = kegiatan_object.nama.replace(" ", "-")
            kegiatan_form.save() #Baru disave akhrinya
        return redirect(reverse("story6_app:detail_kegiatan", args=[nama_kegiatan]))
    # Kalo request.method == "GET"
    context = {
        "kegiatan_form" : KegiatanForm,
        "is_tambah_kegiatan" : True,
        "is_story6_app" : True
    }
    return render(request, "story6_app/tambah_kegiatan.html", context)

def hapus_peserta(request):
    if request.method == "POST":
        try:
            # Coba kita ambil object pesertanya
            peserta = Peserta.objects.get(nama=request.POST["nama"])
            kegiatan = Kegiatan.objects.get(id=request.POST["kegiatan_id"])
        except:
            # Kalo masuk sini, tandanya gaketemu. Langsung redirect ke index aja.
            messages.add_message(request, messages.WARNING, "Peserta {} tidak terdaftar di kegiatan {}".format(request.POST["nama"], request.POST["kegiatan_id"]))
            return redirect("story6_app:index")
        # Kalau gamasuk except, pasti ada peserta di dalam suatu kegiatan
        kegiatan.peserta.remove(peserta) # Hapus peserta yang ikut kegiatan tersebut
        messages.add_message(request, messages.SUCCESS, "Peserta {} sudah dihapus dari {}".format(peserta.nama, kegiatan.nama))
    # redirect("/")
    return redirect("story6_app:index")

def detail_peserta(request, nama):
    nama = nama.replace("-", " ")
    peserta = get_object_or_404(Peserta, nama=nama)
    context = {
        "peserta" : peserta,
        "is_story6_app" : True
    }
    return render(request, "story6_app/detail_peserta.html", context)

def detail_kegiatan(request, nama):
    nama = nama.replace("-", " ")
    kegiatan = get_object_or_404(Kegiatan, nama=nama)
    context = {
        "kegiatan" : kegiatan,
        "peserta_form" : PesertaForm,
        "is_story6_app" : True
    }
    return render(request, "story6_app/detail_kegiatan.html", context)

