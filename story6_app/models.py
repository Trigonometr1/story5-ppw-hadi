from django.db import models
from django.shortcuts import reverse

# Create your models here.

class Peserta(models.Model):
    nama = models.TextField(max_length=50, unique=True)

    def __str__(self):
        return self.nama
    
    def get_absolute_url(self):
        nama_link = self.nama.replace(" ", "-")
        return reverse("story6_app:detail_peserta", args=[nama_link])
    
class Kegiatan(models.Model):
    nama = models.TextField(max_length=70, unique=True)
    peserta = models.ManyToManyField(Peserta, blank=True)

    def __str__(self):
        return self.nama
    
    def get_absolute_url(self):
        nama_link = self.nama.replace(" ", "-")
        return reverse("story6_app:detail_kegiatan", args=[nama_link])
    
