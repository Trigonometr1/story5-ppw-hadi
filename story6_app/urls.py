from django.urls import path
from . import views

app_name = "story6_app"

urlpatterns = [
    path("", views.index, name="index"),
    path("tambah_peserta/<int:id>", views.tambah_peserta, name="tambah_peserta"),
    path("tambah_kegiatan/", views.tambah_kegiatan, name="tambah_kegiatan"),
    path("hapus_peserta/", views.hapus_peserta, name="hapus_peserta"),
    path("detail_peserta/<str:nama>", views.detail_peserta, name="detail_peserta"),
    path("detail_kegiatan/<str:nama>", views.detail_kegiatan, name="detail_kegiatan")
]